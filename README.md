# Buffer grep patch for Vim/Homebrew (depreciated)

*Originally by [Christian Brabandt](https://github.com/chrisbra/vim-mq-patches)*

To build a patched version of Vim follow this routine:

  - update Homebrew;
  - save the original formula file as `vim.rb.orig`;
  - apply the [patch](https://bitbucket.org/dsjkvf/vim-bvimgrep-homebrew/src/master/vimformula.patch) to the Homebrew formula for Vim (i.e., run `patch -b -f vimformula.patch` in the directory the file `vim.rb` is stored in (which is, probably, `/usr/local/Homebrew/Library/Taps/homebrew/homebrew-core/Formula`));
  - use Homebrew as usual (e.g., `brew install vim --with-lua --with-bvimgrep`).
